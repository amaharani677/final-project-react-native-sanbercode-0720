import React, {Component} from 'react';
import { View, StyleSheet, ImageBackground, Text, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import bgImage from './images/background.png'
import Icon from 'react-native-vector-icons/Fontisto';

const {height:HEIGHT} = Dimensions.get('window');
const {width:WIDTH} = Dimensions.get('window');
export default class Login extends Component{
    constructor(props) {
        super(props)
        this.state = {
          email: '',
          password: '',
          isError: false,
        }
      }
    
      loginHandler(){
        console.log(this.state.email, ' ', this.state.password)
        if(this.state.password =='Berusaha'){
            this.setState({isError: false})
            this.props.navigation.navigate('Beranda',{
              screen: 'Makanan'
            })
        } else {
            this.setState({isError: true})
            alert('Password Salah! Coba Berusaha')
          }
      }
    render(){
        return(
            <ImageBackground source={bgImage} style={styles.bgContainer}>
                <View style={{right: 32}}>
                    <Text style={styles.judul}>Selamat Datang di</Text>
                    <Text style={styles.nama}>QUY EAT !</Text>
                </View>
                <View style={styles.box}>
                    <View>
                        <Text style={styles.subJudul}>Login</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.inputJudul}>Email</Text>
                        <TextInput
                        style={styles.input}
                        placeholder = 'abc@gmail.com'
                        placeholderTextColor = 'rgba(196, 196, 196, 0.8)'
                        onChangeText={email => this.setState({ email })}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.inputJudul}>Password</Text>
                        <TextInput
                        style={styles.input}
                        placeholder= '********'
                        secureTextEntry={true}
                        placeholderTextColor= 'rgba(196, 196, 196, 0.8)'
                        onChangeText={password => this.setState({ password })}
                        />
                        <TouchableOpacity style={styles.btnEye}>
                            <Icon style={{color:'#10202F'}} name="eye" size={20} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.btnMasuk} onPress={() => this.loginHandler()}>
                        <Text style={styles.btnText}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.ket}>Belum punya akun?</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <Text style={styles.tab}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    bgContainer: {
        flex: 1,
        height: HEIGHT,
        padding: 30,
        alignItems: 'center'
    },
    judul: {
        fontFamily: 'Roboto',
        fontSize: 36,
        fontWeight: 'bold',
        color: '#EE9623',
        lineHeight: 42
    },
    nama: {
        fontFamily: 'Roboto',
        fontSize: 48,
        fontWeight: 'bold',
        color: '#EE9623',
        lineHeight: 56
    },
    box: {
        height: HEIGHT-368,
        width: WIDTH-136,
        backgroundColor: 'rgba(196, 196, 196, 0.8)',
        borderRadius: 25,
        top: 42,
        padding: 20
    },
    subJudul: {
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#10202F',
        lineHeight: 28,
        textAlign: 'center',
        paddingBottom: 30
    },
    inputContainer: {
        paddingBottom: 15
    },
    inputJudul: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#000000',
        lineHeight: 19
    },
    input: {
        backgroundColor:'#E5E5E5',
        height: 35,
        paddingLeft: 7
    },
    btnEye: {
        position: 'absolute',
        right: 10,
        bottom: 22
    },
    btnMasuk: {
        width: 100,
        height: 30,
        borderRadius: 5,
        backgroundColor:'#10202F',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginHorizontal: 48
    },
    btnText: {
        fontFamily: 'Roboto',
        fontSize: 24,
        color: '#ffffff',
        lineHeight: 28,
    },
    ket: {
        fontFamily: 'Roboto',
        fontSize: 14,
        lineHeight: 16,
        color: '#10202F',
        textAlign: 'center',
        top: 7
    },
    tab: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        lineHeight: 16,
        color: '#10202F',
        textAlign: 'center',
        top: 10
    }
})