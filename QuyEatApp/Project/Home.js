import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Login from './Login';
import Register from './Register';
import Makan from './Makanan';
import Minum from './Minuman';
import Shop from './Shop';
import About from './About';
import {createStore} from 'redux';
import {Provider} from 'react-redux';


const globalState = {
    totalOrder: 0
}
const rootReduces = (state = globalState, action) =>{
    if(action.type === 'PLUS_ORDER'){
        return {
            ...state,
            totalOrder: state.totalOrder + 1
        }
    }
    else if(action.type === 'MINUS_ORDER'){
        let totalOrder = 0
        if(state.totalOrder > 0){
            totalOrder = state.totalOrder - 1
        }
        return {
            ...state,
            totalOrder: totalOrder
        }
    }
    return state;
}
const storeRedux = createStore(rootReduces);


const LoginStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={Login} />
        <LoginStack.Screen name="Beranda" component={DrawerScreen} />
        <LoginStack.Screen name="Register" component={Register} />
    </LoginStack.Navigator>
);
const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Makanan" component={Makan} />
        <Drawer.Screen name="Minuman" component={Minum} />
        <Drawer.Screen name="Belanja" component={Shop} />
        <Drawer.Screen name="Tentang" component={About} />
    </Drawer.Navigator>
);

export default function App(){
    return (
        <Provider store={storeRedux}>
        <NavigationContainer>
            <LoginStackScreen/>
        </NavigationContainer>
        </Provider>
    );
}