import React, {Component} from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, ScrollView, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import {connect} from 'react-redux';

class About extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.barAtas}>
                    <Text style={styles.nama}>QUY EAT</Text>
                </View>
                <ScrollView>
                <View style={styles.perkenalan}>
                    <Image style={styles.gambar} source={require('./images/Maharani.jpg')} />
                    <Text style={styles.name}>Ajeng Ayu Sri Maharani</Text>
                    <Text style={styles.ket}>React Native Developer</Text>
                </View>
                <View style={styles.boxA}>
                    <View style={styles.contact}>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://api.instagram.com/ajengayusri')}>
                                <Icon style={{margin:10, color: '#FFFFFF'}} name="instagram" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>@ajengayusri</Text>
                        </View>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://api.whatsapp.com/send?phone=6287850600922&text=Halo')}>
                                <Icon style={{margin: 10, color: '#FFFFFF'}} name="whatsapp" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>Ajeng Maharani</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.boxA}>
                    <View style={styles.contact}>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://gitlab.com/amaharani677')}>
                                <Icon style={{margin:10, color: '#FFFFFF'}} name="gitlab" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>@amaharani677</Text>
                        </View>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('http://kadobeda.mobirisesite.com/')}>
                                <Icon style={{margin: 10, color: '#FFFFFF'}} name="chrome" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>http://kadobeda.mobirisesite.com/</Text>
                        </View>
                    </View>
                </View>
                </ScrollView>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Makanan')}>
                        <Icon name="home" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Belanja')}>
                        <Text style={{color: '#FFFFFF'}}>{this.props.order}</Text>
                        <Icon name="shopping-basket" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="person" size={25} style={{color: '#EE9623'}} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        order: state.totalOrder
    }
}
export default connect(mapStateToProps)(About);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    barAtas: {
        height: 65,
        backgroundColor: '#10202F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nama: {
        fontFamily: 'Roboto',
        fontSize: 48,
        fontWeight: 'bold',
        color: '#EE9623',
        lineHeight: 56
    },
    perkenalan: {
        margin: 30,
        alignItems: 'center'
    },
    gambar: {
        width: 165,
        height: 165,
        borderRadius: 50
    },
    name: {
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: '500',
        color: '#000000',
        lineHeight: 28,
        marginTop: 28
    },
    ket: {
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight: '500',
        color: '#10202F',
        lineHeight: 21
    },
    boxA: {
        height: 120,
        backgroundColor: '#10202F',
        borderRadius: 25,
        marginHorizontal: 38,
        marginBottom: 15
    },
    contact: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    info: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    username: {
        fontFamily: 'Roboto',
        fontSize: 10,
        fontWeight: '500',
        color: '#F2F2F2',
        lineHeight: 12
    },
    tabBar: {
        backgroundColor: '#10202F',
        height: 60,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        elevation: 4
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})