import React, {Component} from 'react';
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import {connect} from 'react-redux';

class Shop extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.barAtas}>
                    <Text style={styles.nama}>QUY EAT</Text>
                </View>
                <View style={styles.box}>
                    <View>
                        <Text style={styles.inputJudul}>Alamat</Text>
                        <TextInput
                        style={styles.input}
                        placeholder = 'Jalan, Kelurahan, Kecamatan, Kota, Provinsi'
                        placeholderTextColor = 'rgba(196, 196, 196, 0.8)'
                        />
                    </View>
                    <TouchableOpacity style={styles.btn}>
                        <Text style={styles.btnText}>Lihat Pesanan</Text>
                    </TouchableOpacity>
                    <View style={{marginTop:40}}>
                    <Text style={styles.inputJudul}>Cara Pembayaran</Text>
                    <View style={styles.boxPay}>
                        <TouchableOpacity style={styles.btnPay}>
                            <Text style={styles.btnText}>OVO</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnPay}>
                            <Text style={styles.btnText}>DANA</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.boxPay}>
                        <TouchableOpacity style={styles.btnPay}>
                            <Text style={styles.btnText}>GOPAY</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnPay}>
                            <Text style={styles.btnText}>CASH</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.note}>*Gratis ongkir Denpasar Selatan dengan minimum order 10pcs</Text>
                    <TouchableOpacity style={styles.btn}>
                        <Text style={styles.btnText}>Pesan</Text>
                    </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Makanan')}>
                        <Icon name="home" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Text style={{color: '#FFFFFF'}}>{this.props.order}</Text>
                        <Icon name="shopping-basket" size={25} style={{color: '#EE9623'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Tentang')}>
                        <Icon name="person" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        order: state.totalOrder
    }
}
export default connect(mapStateToProps)(Shop);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    barAtas: {
        height: 65,
        backgroundColor: '#10202F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nama: {
        fontFamily: 'Roboto',
        fontSize: 48,
        fontWeight: 'bold',
        color: '#EE9623',
        lineHeight: 56
    },
    box: {
        flex: 1,
        paddingHorizontal: 48,
        paddingVertical: 40
    },
    inputJudul: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#000000',
        lineHeight: 16
    },
    input: {
        backgroundColor:'#E5E5E5',
        height: 82,
        paddingLeft: 7,
        paddingBottom: 40,
        marginBottom: 40
    },
    btn: {
        width: 276,
        height: 37,
        borderRadius: 10,
        backgroundColor:'#EE9623',
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxPay: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingHorizontal: 30
    },
    btnPay: {
        width: 134,
        height: 37,
        borderRadius: 10,
        backgroundColor:'#10202F',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginHorizontal: 48
    },
    btnText: {
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#ffffff',
        lineHeight: 28,
    },
    note: {
        paddingTop:40,
        paddingBottom: 10,
        fontFamily: 'Roboto',
        fontSize: 10,
        color: '#10202F',
        lineHeight: 12
    },
    tabBar: {
        backgroundColor: '#10202F',
        height: 60,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        elevation: 4
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})