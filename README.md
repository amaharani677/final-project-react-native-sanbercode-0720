Final project dibuat oleh Ajeng Ayu Sri Maharani

Project React Native yang dibuat merupakan sebuah aplikasi untuk berjualan makanan / minuman dengan beberapa halaman meliputi: Login, Register, Beranda (makanan, minuman, belanja, dan profil). React Navigation 5 digunakan pada halaman login/register ke beranda menggunakan stack, dan pada halaman beranda terdapat navigasi Drawer. Redux digunakan pada jumlah orderan.
Saya kurang paham API apa saja yang digunakan, yang saya tau terdapat API Instagram dan Whatsapp.
Mockup Figma : https://www.figma.com/file/QgPsiSVDwA7a61WvzwkLog/Untitled?node-id=0%3A1 
Gitlab : https://gitlab.com/amaharani677/final-project-react-native-sanbercode-0720/-/commit/4ecd122339d0424693c892a034133facbd32cd65
GDrive: https://drive.google.com/drive/folders/1uN9p0tSfaDtblYvzqT5XykEMwRNhvyhc?usp=sharing